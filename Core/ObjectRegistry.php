<?php
namespace Core\Objects;
class ObjectRegistry{
	
	static  $objects = array();

	Static function load($Class = ""){
		if(self::__is_loaded($Class)){
			return self::$objects[$Class];
		}else{
			return self::$objects[$Class] = new $Class();
		}
		
	}
	
	Static private function __is_loaded($Class){
		$is_loaded = false;
		foreach(self::$objects as $_class=>$object){
			if($_class==$Class){
				$is_loaded = true;
				break;
			}
		}
		return $is_loaded;
	}
}

?>
