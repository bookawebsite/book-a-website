<?php

define("DS",'/'); /*Directory Separtor*/


include_once "Core/functions.php";
include_once "Core/Controller/Controller.php";
include_once "Core/Route/Request.php";
include_once "Core/ObjectRegistry.php";

use Core\Route;

$request_uri = "";
if(!empty($_SERVER['REQUEST_URI'])){
	$break = explode(DS,$_SERVER['SCRIPT_NAME']);
	$break2 = explode(DS,$_SERVER['REQUEST_URI']);
	$result = array_diff($break2, $break);
	$request_uri = implode(DS, $result);
}

$controller = new Core\Route\Request($request_uri);


?>
